// NameMaker.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"
int counter = 0;

struct Name
{
	char name[256];
};
int zufallsGen(int MAX)
{
	return rand() % MAX;
}
void vornamenGen(Name *names) 
{
	// Variablen
	char vorname[128];
	char nachname[128];
	int z = zufallsGen(70); // Neue Zufallszahl basierend auf der Anzahl der Vornamen.

	FILE *fileVN;
	FILE *fileNN;

	fileNN = fopen("Nachnamen.txt", "r");
	fileVN = fopen("Vornamen.txt", "r");

	if (fileVN == NULL)
	{
		printf("Fehler beim Einlesen der Vornamen!\n");
	}
	else
	{
		int i;
		for (i = 0; i <= z; i++)
		{
			fgets(vorname, 128, fileVN);
		}
	}

	z = zufallsGen(3000); // Neue Zufallszahl basierend auf der Anzahl der Nachnamen.

	if (fileNN == NULL)
	{
		printf("Fehler beim Einlesen der Nachnamen!\n");
	}
	else
	{
		int i;
		for (i = 0; i <= z; i++)
		{
			fgets(nachname, 128, fileNN);
		}
	}

	// Stringfunktionen welche die gelesenen Namen bearbeiten. 
	// STRTOK == Zeilenumbr�che l�schen welche durch fgets verursacht werden.
	// STRCPY kopiert "vornamen" in "namen".
	// STRCAT h�ngt "namen" ein Leerzeichen und "nachname" an.
	strtok(vorname, "\n");
	strtok(nachname, "\n");

	strcpy((*names).name, vorname);
	strcat((*names).name, " ");
	strcat((*names).name, nachname);

	fclose(fileNN);
	fclose(fileVN);
}
void kontrolle(Name *names)
{
	FILE *fileName;
	char Namen[256];
	bool k = true;

	
	

	fileName = fopen("Namen.txt", "r");
	if (fileName == NULL)
	{
		printf("Fehler beim �ffnen der Namensdatei.");
	}
	else
	{
		while (fgets(Namen, 256, fileName) != NULL && k == true)
		{
			strtok(Namen, "\n");
			if (strcmp(Namen, (*names).name) == 0)
			{
				counter += 1;
				k = false;
			}
		}
		fclose(fileName);
		if (k == true)
		{
			fileName = fopen("Namen.txt", "a");
			fprintf(fileName, "%s\n", (*names).name);
			fclose(fileName);
		}
	}

}



int main()
{
	Name names; 
	srand(time(NULL));
	int x = 0;
	while (x < 500)
	{
		vornamenGen(&names);
		//printf("%s\n", names.name);
		kontrolle(&names);
		++x;
	}
	printf("Es wurden %d doppelte Namen erzeugt.", counter);
	getchar();
	return 0;
}